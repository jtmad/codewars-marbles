﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter( Collision collision)
	{
		Vector3 pos = transform.position;
		if (collision.collider.name == "Bottom Wall") {
			this.transform.position = new Vector3(2.853072f, 7.122854f, pos.z);
		}
		else if (collision.collider.name == "Top Wall"){
			this.transform.position = new Vector3(2.853072f, 7.122854f, pos.x);
		}
	}
}
