 using UnityEngine;
 using System.Collections;
 using System.IO;
 public class Combo2 : MonoBehaviour
 {
	GUIContent[] comboBoxList;
	private ComboBox comboBoxControl = new ComboBox();
	private GUIStyle listStyle = new GUIStyle();

	private void Start()
	{
	    comboBoxList = new GUIContent[2];
	    comboBoxList[0] = new GUIContent("Sky");
		comboBoxList[1] = new GUIContent("Steel");

		comboBoxControl.SetSelectedItemIndex(1);
	    listStyle.normal.textColor = Color.white; 
	    listStyle.onHover.background =
	    listStyle.hover.background = new Texture2D(2, 2);
	    listStyle.padding.left = 1;
	    listStyle.padding.right = 
	    listStyle.padding.top = 1;
	    listStyle.padding.bottom = 4;
	}

	private void OnGUI () 
	{
	    int selectedItemIndex = comboBoxControl.GetSelectedItemIndex();
	    selectedItemIndex = comboBoxControl.List( 
			new Rect(5, 200, 120, 20), comboBoxList[selectedItemIndex].text, comboBoxList, listStyle );
        GUI.Label( new Rect(5, 180, 100, 20), 
			"Player 2 Color");
	}
	
	private void Update()
	{
		Texture Sky = (Texture)Resources.LoadAssetAtPath("Assets/skyBox.tga", typeof(Texture));
		Texture Steel = (Texture)Resources.LoadAssetAtPath("Assets/Standard Assets (Mobile)/Textures/JoystickThumb.psd", typeof(Texture));
		int select = comboBoxControl.GetSelectedItemIndex();
		if (select == 0)
			GameObject.Find("Sphere2").renderer.material.mainTexture = Sky;
		if (select == 1)
			GameObject.Find("Sphere2").renderer.material.mainTexture = Steel;
	}
 }